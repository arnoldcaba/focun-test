import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAuthService } from './auth.service.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements IAuthService{

  constructor(private httpClient: HttpClient) { }

  login(username: string, password: string[]) {
    return this.httpClient.post('http://localhost:8080/login', {username, password});
  }
}
