import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { IAuthService } from './auth.service.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements IAuthService{

  login(usuario: string, password: string[]) {
    if (usuario === 'juan' && ['dog', 'frog', 'cat'].every((val) => password.includes(val))) {
      return of({
        token: 'fake-jwt-token'
      });
    }
    return of(null);
  }
}
