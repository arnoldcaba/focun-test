import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PassIconsComponent } from './pass-icons.component';

describe('PassIconsComponent', () => {
  let component: PassIconsComponent;
  let fixture: ComponentFixture<PassIconsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PassIconsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PassIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
