import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray } from '@angular/forms';
import { faDog, faCrow, faCat, faDove, faDragon, faFish, faFrog, faHorse, faSpider, faKiwiBird } from '@fortawesome/free-solid-svg-icons'
import { ToastrService } from 'ngx-toastr';
import { AnimalIcon } from '../interfaces/animalIcon.interface';

@Component({
  selector: 'app-pass-icons',
  templateUrl: './pass-icons.component.html',
  styleUrls: ['./pass-icons.component.css']
})


export class PassIconsComponent implements OnInit {

  @Input() passArray: FormArray;
  @Output() clicked: EventEmitter<AnimalIcon> = new EventEmitter<AnimalIcon>();
  faCoffee = faCat;
  defColor: string = 'lightgray';
  animals: AnimalIcon[] = [
    { nombre: 'dog', icono: faDog, color: this.defColor },
    { nombre: 'crow', icono: faCrow, color: this.defColor },
    { nombre: 'cat', icono: faCat, color: this.defColor },
    { nombre: 'dove', icono: faDove, color: this.defColor },
    { nombre: 'dragon', icono: faDragon, color: this.defColor },
    { nombre: 'fish', icono: faFish, color: this.defColor },
    { nombre: 'frog', icono: faFrog, color: this.defColor },
    { nombre: 'horse', icono: faHorse, color: this.defColor },
    { nombre: 'spider', icono: faSpider, color: this.defColor },
    { nombre: 'kiwi', icono: faKiwiBird, color: this.defColor }
  ];
  colors: string[] = ['#F44336', '#E91E63', '#9C27B0', '#3F51B5', '#2196F3', '#00BCD4', '#4CAF50', '#CDDC39', '#FF9800'];

  constructor(private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.animals = this.animals.sort((a, b) => Math.random() - 0.5)
  }

  onClick (animal: AnimalIcon) {
    if (this.passArray.value.length < 3) {
      const found = this.passArray.value.find(item => item.nombre === animal.nombre);
      if (!found) {
        // obteniendo colores tomados
        const takenColors = this.passArray.value.reduce((acc, item) => { return acc.concat(item.color) }, []);
        // filtrando el array de colores para que no se repitan
        const colors = this.colors.filter(color => !takenColors.includes(color));
        // seleccionando un color al azar
        const randomColor = colors[Math.floor(Math.random() * colors.length)];
        animal.color = randomColor;
        // emitir el animal
        this.clicked.emit(animal);
      } else {
        this.toastr.info('Ya marcaste este animal!', 'Ya seleccionado!');
      }
    } else {
      this.toastr.warning('Ya seleccionaste 3 animales!', 'Ya seleccionaste 3!');
    }
  }
  reset () {
    console.log('reset');
    this.animals.map(animal => {
      animal.color = this.defColor;
    })
  }

}
