import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AnimalIcon } from '../interfaces/animalIcon.interface';
import { PassIconsComponent } from '../pass-icons/pass-icons.component';
import { AuthService } from '../services/auth.mock.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChildren(PassIconsComponent)
  compHijos: QueryList<PassIconsComponent>

  get getPassArray() {
    return this.loginForm.get('passArray') as FormArray;
  };

  get progress () {
    return this.getPassArray.value.length * 100 / 3;
  };

  loginForm: FormGroup = this.fb.group({
    usuario: ['', Validators.required],
    passArray: this.fb.array([], [Validators.required, Validators.minLength(3)])
  });

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private _authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit () {
    if (this.loginForm.valid) {
      // seteando los parametros
      const usuario = (this.loginForm.get('usuario').value).toLowerCase().trim();
      const passArray = this.getPassArray.value.reduce((acc, item) => { return acc.concat(item.nombre)}, []);
      // llamando al servicio MOCK
      this._authService.login(usuario, passArray).subscribe((res) => {
        console.log(res);
        if (res) {
          this.toastr.success(`Login exitoso! Tu token es: ${res.token}`, 'Bienvenido!');
        } else {
          this.toastr.error('Usuario o contraseña incorrectos!', 'Error!');
        }
      }, err => {
        console.log(err);
        this.toastr.error('Usuario o contraseña incorrectos!', 'Error!');
      })
    }
  }
  selectedImg(animal: AnimalIcon) {
    // console.log(amimal);
    if (this.getPassArray.value.length < 3) {
      this.getPassArray.push(this.fb.control(animal));
    } else {
      this.toastr.warning('Ya seleccionaste 3 animales!', 'Ya seleccionaste 3!');
    }
  }
  clear () {
    this.loginForm.get('usuario').reset(); // limpio el input
    this.getPassArray.clear(); // limpio el array de iconos seleccionados
    this.compHijos.forEach(c => c.reset()); // reseteo el componente hijo
  }

}
